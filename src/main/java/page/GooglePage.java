package page;

import lombok.Data;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

@Data
public class GooglePage {
    private WebDriver driver;

    public GooglePage(WebDriver driver) {
        this.driver = driver;
    }

    public GooglePage openPage() {
        driver.get("https://www.google.com");
        return this;
    }

    public GooglePage assertThatCookiesPoliticsPageIsDisplayed() {
        assertThat(driver.findElement(By.id("S3BnEe")).isDisplayed()).isTrue();
        return this;
    }
}
