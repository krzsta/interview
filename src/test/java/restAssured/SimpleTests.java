package restAssured;

import org.testng.annotations.Test;

import static config.RestAssuredConfig.getRequestSpecification;
import static io.restassured.RestAssured.given;

public class SimpleTests {

    @Test
    public void test1() {
        given()
                .spec(getRequestSpecification())
                .get("https://jsonplaceholder.typicode.com/users")
                .prettyPeek();
    }
}
