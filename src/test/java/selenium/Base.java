package selenium;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import page.GooglePage;

public class Base {
    protected WebDriver driver;

    @BeforeTest
    public void setUpBeforeTest() {
        var options = new ChromeOptions();
        options.addArguments("--no-sandbox");

        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver(options);
        driver.manage().window().maximize();
    }

    @AfterTest
    public void cleanUpAfterTest() {
        driver.quit();
    }

    protected GooglePage openGooglePage() {
        return new GooglePage(driver).openPage();
    }
}
