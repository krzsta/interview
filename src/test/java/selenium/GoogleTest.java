package selenium;

import org.testng.annotations.Test;

public class GoogleTest extends Base {

    @Test
    public void shouldCookiesPolicyBeDisplayedWhenOpenGoogleMainPage() {
        openGooglePage()
                .assertThatCookiesPoliticsPageIsDisplayed();
    }
}
